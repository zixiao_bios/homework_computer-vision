#include "../include/imgFilter.h"

/// 计算图像卷积
/// \param img 原始图像
/// \param kernel 卷积核
/// \param padding 是否进行边缘填充
/// \return 处理后的图像
Mat imgFilter::convolution(const Mat &img, const Mat &kernel, bool padding) {
    int wBegin = (kernel.size().width - 1) / 2;
    int wEnd = img.size().width - wBegin;
    int hBegin = (kernel.size().height - 1) / 2;
    int hEnd = img.size().height - hBegin;

    Mat newImg(img.size(), CV_8UC3);

    // 卷积
    for (int i = hBegin; i < hEnd; ++i) {
        for (int j = wBegin; j < wEnd; ++j) {
            Vec3d p = {0, 0, 0};
            for (int k = 0; k < kernel.size().height; ++k) {
                for (int l = 0; l < kernel.size().width; ++l) {
                    p += kernel.at<double>(k, l) * (Vec3d) img.at<Vec3b>(i + k - hBegin, j + l - wBegin);
                }
            }
            newImg.at<Vec3b>(i, j) = p;
        }
    }

    if (padding) {
        // 边缘填充
        // 上
        for (int i = 0; i < hBegin; ++i) {
            for (int j = wBegin; j < wEnd; ++j) {
                newImg.at<Vec3b>(i, j) = newImg.at<Vec3b>(2 * hBegin - i - 1, j);
            }
        }

        // 下
        for (int i = hEnd; i < img.size().height; ++i) {
            for (int j = wBegin; j < wEnd; ++j) {
                newImg.at<Vec3b>(i, j) = newImg.at<Vec3b>(2 * hEnd - i - 1, j);
            }
        }

        // 左
        for (int i = 0; i < newImg.size().height; ++i) {
            for (int j = 0; j < wBegin; ++j) {
                newImg.at<Vec3b>(i, j) = newImg.at<Vec3b>(i, 2 * wBegin - j - 1);
            }
        }

        // 右
        for (int i = 0; i < img.size().height; ++i) {
            for (int j = wEnd; j < img.size().width; ++j) {
                newImg.at<Vec3b>(i, j) = newImg.at<Vec3b>(i, 2 * wEnd - j - 1);
            }
        }
    }

    return newImg;
}

/// 计算高斯卷积核
/// \param sigma 标准差
/// \return 高斯卷积核
Mat imgFilter::gaussKernel(int sigma) {
    Mat gauss = Mat::zeros(2 * sigma + 1, 2 * sigma + 1, CV_64FC1);
    for (int i = -sigma; i < sigma + 1; ++i) {
        for (int j = -sigma; j < sigma + 1; ++j) {
            gauss.at<double>(i + sigma, j + sigma) = exp(-0.5 * (i * i + j * j) / (sigma * sigma));
        }
    }
    double s = sum(gauss)[0];
    gauss /= s;
    
    return gauss;
}

/// 计算单位冲击矩阵，即中央元素为1，其余均为0
/// \param size 矩阵的行数和列数
/// \return 单位冲击矩阵
Mat imgFilter::unitKernel(int size) {
    Mat unit = Mat::zeros(size, size, CV_64FC1);
    unit.at<double>((size - 1) / 2, (size - 1) / 2) = 1;
    return unit;
}

/// 计算拉普拉斯卷积核，方法：单位冲击矩阵-高斯核
/// \param sigma 标准差，即高斯核的sigma
/// \return 拉普拉斯卷积核
Mat imgFilter::laplacianKernel(int sigma) {
    Mat laplacian = imgFilter::gaussKernel(sigma);
    laplacian = imgFilter::unitKernel(laplacian.size().width) - laplacian;
    return laplacian;
}

Mat imgFilter::averageKernel(int size) {
    Mat kernel = Mat::ones(size, size, CV_64FC1);
    kernel /= kernel.size().width * kernel.size().height;
    return kernel;
}
