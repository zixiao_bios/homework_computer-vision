#ifndef HOMEWORK_COMPUTERVISION_IMGFILTER_H
#define HOMEWORK_COMPUTERVISION_IMGFILTER_H

#include <opencv2/core.hpp>

using namespace std;
using namespace cv;

class imgFilter {
public:
    static Mat convolution(const Mat &img, const Mat &kernel, bool padding = true);

    static Mat gaussKernel(int sigma);

    static Mat unitKernel(int size);

    static Mat laplacianKernel(int sigma);

    static Mat averageKernel(int size);
};


#endif //HOMEWORK_COMPUTERVISION_IMGFILTER_H
