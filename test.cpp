#include <iostream>
#include <omp.h>
#include <vector>
#include <numeric>

using namespace std;

int main(){
    int stepNum = 1000000000;
    int threadNum = 10;
    double step = 1.0 / (double) stepNum;
    vector<double> sum(threadNum);

#pragma omp parallel default(none) shared(stepNum, threadNum, step, sum) num_threads(threadNum)
    {
        double x;
        double sum_t=0.0;
        int id = omp_get_thread_num();
        for (int i = id; i < stepNum; i+=threadNum) {
            x = (i - 0.5) * step;
            sum_t += 4.0 / (1.0 + x * x);
        }
        sum[id] = sum_t;
    }

    double pi = accumulate(sum.begin(), sum.end(), 0.0) * step;
    cout << "pi=" << pi << endl;
}