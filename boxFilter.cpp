#include <iostream>
#include <opencv2/highgui.hpp>
#include "include/imgFilter.h"

using namespace std;
using namespace cv;

// 图片路径
string path = "../img/01_mid.jpg";

int main(){
    Mat img = imread(path);

    // 均值滤波
    Mat kernel = imgFilter::averageKernel(5);

    // 高斯滤波
//    Mat kernel = imgFilter::gaussKernel(3);

    // 锐化
//    Mat kernel = (Mat_<double>(3,3) <<
//            -1, -1, -1,
//            -1, 9, -1,
//            -1, -1, -1);

    // Prewitt算子
//    Mat kernel = (Mat_<double>(3,3) <<
//            -1, 0, 1,
//            -1, 0, 1,
//            -1, 0, 1);

    // Laplace算子
//    Mat kernel = imgFilter::laplacianKernel(5);

    cout << kernel << endl;

    imshow("img", img);
    imshow("filtering", imgFilter::convolution(img, kernel));

    waitKey(-1);
    return 0;
}