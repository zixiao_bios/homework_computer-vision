#include <iostream>
#include <opencv2/highgui.hpp>
#include "include/imgFilter.h"

using namespace std;
using namespace cv;

// Laplacian滤波图片路径
string path1 = "../img/02.png";

// Gaussian滤波图片路径
string path2 = "../img/03.png";

// Laplacian滤波标准差
int sigmaLap = 10;

// Gaussian滤波标准差
int sigmaGau = 6;

int main(){
    Mat laplacian = imgFilter::laplacianKernel(sigmaLap);
    Mat gaussian = imgFilter::gaussKernel(sigmaGau);

    Mat img1 = imread(path1);
    Mat img2 = imread(path2);

    Mat img1a = imgFilter::convolution(img1, laplacian);
    Mat img2a = imgFilter::convolution(img2, gaussian);

    imshow("Laplacian", img1a);
    imshow("Gaussian", img2a);

    Mat hybridImg = img1a + img2a;
    imshow("HybridImg", hybridImg);

    waitKey(-1);
    return 0;
}