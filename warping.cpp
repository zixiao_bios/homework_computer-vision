#include <iostream>
#include <omp.h>
#include <opencv2/highgui.hpp>

using namespace std;
using namespace cv;

// 显示的图片路径
string path = "../img/01_small.jpg";

// 缩放百分比，初始为100
int zoom = 100;

// 旋转角度，使用角度制，初始为0
int rotateAngle = 0;

Mat img;

/// 使用最近邻算法缩放图片
/// \param origin 原始图片
/// \param size 目标分辨率
/// \return 缩放后的图片
Mat zoomNearest(const Mat& origin, const Size& size) {
    Size originSize = origin.size();
    vector<int> transferX(size.width), transferY(size.height);
    double bl;

    // 计算x、y的坐标转移向量
    for (int i = 0; i < size.width; ++i) {
        bl = (double)i / size.width;
        transferX[i] = round(bl * originSize.width);
    }
    for (int i = 0; i < size.height; ++i) {
        bl = (double) i / size.height;
        transferY[i] = round(bl * originSize.height);
    }

    Mat after(size, CV_8UC3);
    for (int i = 0; i < size.height; ++i) {
        for (int j = 0; j < size.width; ++j) {
            after.at<Vec3b>(i, j) = origin.at<Vec3b>(transferY[i], transferX[j]);
        }
    }

    return after;
}

/// 旋转图片
/// \param origin 原始图片
/// \param angle 逆时针旋转的角度，使用弧度制表示
/// \return 旋转后的图片
Mat rotateMultiThreads(const Mat &origin, const double angle){
    // 计算旋转后图片的尺寸
    double newWidth = ceil(abs(origin.size().width * cos(angle)) +
                           abs(origin.size().height * sin(angle)));
    double newHeight = ceil(abs(origin.size().width * sin(angle))
                            + abs(origin.size().height * cos(angle)));
    Size newSize(newWidth, newHeight);

    // 将原图片的原点平移到中央的平移矩阵
    Mat transMat1 = Mat::eye(3, 3, CV_64FC1);
    transMat1.at<double>(1, 1) = -1;
    transMat1.at<double>(2, 0) = -0.5 * origin.size().width;
    transMat1.at<double>(2, 1) = 0.5 * origin.size().height;

    // 将旋转后图片的原点平移到左上角的平移矩阵
    Mat transMat2 = Mat::eye(3, 3, CV_64FC1);
    transMat2.at<double>(1, 1) = -1;
    transMat2.at<double>(2, 0) = 0.5 * newSize.width;
    transMat2.at<double>(2, 1) = 0.5 * newSize.height;

    // 旋转矩阵
    Mat rotMat = Mat::eye(3, 3, CV_64FC1);
    rotMat.at<double>(0, 0) = cos(angle);
    rotMat.at<double>(0, 1) = -sin(angle);
    rotMat.at<double>(1, 0) = sin(angle);
    rotMat.at<double>(1, 1) = cos(angle);

    // 多线程旋转图像
    int threadNum = 4;
    Mat after = Mat::zeros(newSize, CV_8UC3);
#pragma omp parallel default(none) shared(newSize, after, transMat1, rotMat, transMat2, threadNum, origin) num_threads(threadNum)
    {
        Mat originTemp = Mat(origin);
        int id=omp_get_thread_num();
        // 旋转，使用前向映射方法
//        Mat after = Mat::zeros(newSize, CV_8UC3);
        Mat originPos = Mat::ones(1, 3, CV_64FC1);
        Mat newPos = Mat::ones(1, 3, CV_64FC1);
        for (int i = id; i < originTemp.size().height; i+=threadNum) {
            originPos.at<double>(0, 1) = i;
            for (int j = 0; j < originTemp.size().width; ++j) {
                originPos.at<double>(0, 0) = j;
                newPos = originPos * transMat1 * rotMat * transMat2;

                after.at<Vec3b>(round(newPos.at<double>(0, 1)),
                                round(newPos.at<double>(0, 0)))
                        = originTemp.at<Vec3b>(i, j);
            }
        }
    }

    return after;
}

/// 旋转图片
/// \param origin 原始图片
/// \param angle 逆时针旋转的角度，使用弧度制表示
/// \return 旋转后的图片
Mat rotate(const Mat &origin, const double angle){
    // 计算旋转后图片的尺寸
    double newWidth = ceil(abs(origin.size().width * cos(angle)) +
                           abs(origin.size().height * sin(angle)));
    double newHeight = ceil(abs(origin.size().width * sin(angle))
                            + abs(origin.size().height * cos(angle)));
    Size newSize(newWidth, newHeight);

    // 将原图片的原点平移到中央的平移矩阵
    Mat transMat1 = Mat::eye(3, 3, CV_64FC1);
    transMat1.at<double>(1, 1) = -1;
    transMat1.at<double>(2, 0) = -0.5 * origin.size().width;
    transMat1.at<double>(2, 1) = 0.5 * origin.size().height;

    // 将旋转后图片的原点平移到左上角的平移矩阵
    Mat transMat2 = Mat::eye(3, 3, CV_64FC1);
    transMat2.at<double>(1, 1) = -1;
    transMat2.at<double>(2, 0) = 0.5 * newSize.width;
    transMat2.at<double>(2, 1) = 0.5 * newSize.height;

    // 旋转矩阵
    Mat rotMat = Mat::eye(3, 3, CV_64FC1);
    rotMat.at<double>(0, 0) = cos(angle);
    rotMat.at<double>(0, 1) = -sin(angle);
    rotMat.at<double>(1, 0) = sin(angle);
    rotMat.at<double>(1, 1) = cos(angle);

    // 旋转，使用前向映射方法
    Mat after = Mat::zeros(newSize, CV_8UC3);
    Mat originPos = Mat::ones(1, 3, CV_64FC1);
    Mat newPos = Mat::ones(1, 3, CV_64FC1);
    for (int i = 0; i < origin.size().height; i++) {
        originPos.at<double>(0, 1) = i;
        for (int j = 0; j < origin.size().width; ++j) {
            originPos.at<double>(0, 0) = j;
            newPos = originPos * transMat1 * rotMat * transMat2;

            after.at<Vec3b>(round(newPos.at<double>(0, 1)),
                            round(newPos.at<double>(0, 0)))
                    = origin.at<Vec3b>(i, j);
        }
    }

    return after;
}

void onChangeZoom(int, void*){
    double bl = (double) zoom / 100;
    Size newSize(round((double) img.size().width * bl),
                 round((double) img.size().height * bl));
    if (newSize.width == 0 || newSize.height == 0) {
        return;
    }

    imshow("zoom", zoomNearest(img, newSize));
}

void onChangeAngle(int, void*){
    double radAngle = rotateAngle * CV_PI / 180;
    imshow("rotate", rotate(img, radAngle));
}

int main() {
    img = imread(path);

    imshow("img", img);
    imshow("zoom", img);
    imshow("rotate", img);

    createTrackbar("zoom", "img", &zoom, 1000, onChangeZoom);
    createTrackbar("rotate", "img", &rotateAngle, 360, onChangeAngle);

    waitKey(-1);

    return 0;
}