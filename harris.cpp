#include "include/imgFilter.h"
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

using namespace std;
using namespace cv;

// 图片路径
string path = "../img/01_mid.jpg";

double alpha = 0.05;

void drawCornerOnImage(Mat &image, const Mat &binary) {
    Mat_<uchar>::const_iterator it = binary.begin<uchar>();
    Mat_<uchar>::const_iterator itd = binary.end<uchar>();
    for (int i = 0; it != itd; it++, i++) {
        if (*it)
            circle(image, Point(i % image.cols, i / image.cols), 3, Scalar(0, 0, 255), 1);
    }
}

void imshow(const string& name, const Mat& img){
    Mat imgShow;
    img.convertTo(imgShow, CV_8U);
    cv::imshow(name, imgShow);
}

int main(){
    // 加载图片
    Mat img = imread(path);
    imshow("origin", img);

    // 转换数据类型
    Mat imgGray;
    cvtColor(img, imgGray, COLOR_BGR2GRAY);
    imgGray.convertTo(imgGray, CV_64F);

    // 计算x、y方向的梯度
    Mat xKernel = (Mat_<double>(1,3) << -1, 0, 1);
	Mat yKernel = xKernel.t();
    Mat Ix, Iy;
    filter2D(imgGray, Ix, CV_64F, xKernel);
	filter2D(imgGray, Iy, CV_64F, yKernel);

    // 梯度平方
    Mat Ix2,Iy2,Ixy;
	Ix2 = Ix.mul(Ix);
	Iy2 = Iy.mul(Iy);
	Ixy = Ix.mul(Iy);
    imshow("Ixy", Ixy);
    imshow("Ix2", Ix2);
    imshow("Iy2", Iy2);

	// 高斯滤波
	Mat gaussKernel = getGaussianKernel(7, 1);
	filter2D(Ix2, Ix2, CV_64F, gaussKernel);
	filter2D(Iy2, Iy2, CV_64F, gaussKernel);
	filter2D(Ixy, Ixy, CV_64F, gaussKernel);
    imshow("Ixy gauss", Ixy);
    imshow("Ix2 gauss", Ix2);
    imshow("Iy2 gauss", Iy2);

	// 计算角点值
	Mat cornerStrength(imgGray.size(), imgGray.type());
    for (int i = 0; i < imgGray.rows; i++) {
        for (int j = 0; j < imgGray.cols; j++) {
            double det_m = Ix2.at<double>(i, j) * Iy2.at<double>(i, j) - Ixy.at<double>(i, j) * Ixy.at<double>(i, j);
            double trace_m = Ix2.at<double>(i, j) + Iy2.at<double>(i, j);
            cornerStrength.at<double>(i, j) = det_m - alpha * trace_m * trace_m;
        }
    }

    // 显示角点值图像
    imshow("cornerStrength", cornerStrength);

    // 计算角点图
    double maxStrength;
	minMaxLoc(cornerStrength, nullptr, &maxStrength, nullptr, nullptr);
	Mat cornerMap;
	double qualityLevel = 0.001;
	double thresh = qualityLevel * maxStrength;
	cornerMap = cornerStrength > thresh;
    cornerMap.convertTo(cornerMap, CV_8U);

    // 显示结果
    Mat show = Mat(img);
    drawCornerOnImage(show, cornerMap);
    imshow("result", show);

    waitKey(-1);

    return 0;
}